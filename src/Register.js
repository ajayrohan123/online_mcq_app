// import { useState } from 'react';
import Navbar from './Navbar';
import { useForm } from "react-hook-form";
// import './App.css';
export default  function Register() {
  const { register, handleSubmit, formState: { errors } } = useForm();
  const handleRegistration = (data) => console.log(data);
  const handleError = (errors) => {};

  const registerOptions = {
    name: { required: "Name is required" ,
    minLength: {
      value: 4,
      message: "Name must have at least 4 characters"
    }
  },
  phone: { required: "phone is required" ,
  minLength: {
    value: 10,
    message: "phone Number must have at least 10 Numbers"
  }
},
    email: { required: "Email is required" },
    password: {
      required: "Password is required",
      minLength: {
        value: 8,
        message: "Password must have at least 8 characters"
      }
    }
  };

return (
  <div className="login-wrapper">
    <Navbar/>
<div className="container-fluid bg-img row ">

<div className='col-sm-6 offset-sm-3'>
<h1>User Registration</h1>
</div>

<form className='col-sm-6 offset-sm-3' onSubmit={handleSubmit(handleRegistration, handleError)}>
      <div>
        <label>Name</label>
        <input name="name" type="text" placeholder='Enter Name' {...register('name', registerOptions.name) }/>
        <small className="text-danger">
          {errors?.name && errors.name.message}
        </small>
      </div>
      <div>
        <label>Phone</label>
        <input name="phone" type="text" placeholder='Enter Phone' fdprocessedid="tlc5pd" {...register('phone', registerOptions.phone) }/>
        <small className="text-danger">
          {errors?.phone && errors.phone.message}
        </small>
      </div>
      <div>
        <label>Email</label>
        <input
          type="email"
          name="email"
          {...register('email', registerOptions.email)}
        />
        <small className="text-danger">
          {errors?.email && errors.email.message}
        </small>
      </div>
      <div>
        <label>Password</label>
        <input
          type="password"
          name="password"
          placeholder='Enter Password'
          {...register('password', registerOptions.password)}
        />
        <small className="text-danger">
          {errors?.password && errors.password.message}
        </small>
      </div>
      <button className='btn btn-success'>Submit</button>
    </form>
    </div>
    </div>
  );
};

