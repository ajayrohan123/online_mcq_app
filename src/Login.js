import React from 'react'
import Navbar from './Navbar'

//  import { Link } from 'react-router-dom';
import './login.css';

const Login = () => {
  

  // }
  const [formStatus, setFormStatus] = React.useState('Send')
  const onSubmit = (e) => {
    e.preventDefault()
    setFormStatus('Submitting...')
    const { email, psw } = e.target.elements
    let conFom = {
      email: email.value,
      
      psw: psw.value,
    }
    console.log(conFom)
  }
  return (
  <div className="login-wrapper">
    
<Navbar/>

<div className='container-fluid bg-img row'>
  
  <div className='col-sm-8 '>
  
  <h2>hello</h2>
  <p>is simply dummy text of the printing and typesetting industry. 
    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>
  </div>
  
  <form action="" className='col-sm-4' onSubmit={onsubmit}>
  <button class="gradient-light flex justify-center inset-shadow-black text-sm items-center text-dark w-full rounded-sm h-10 sm:h-[40px] space-x-5 sm:space-x-[10px] mb-3 font-semibold" fdprocessedid="3w23da"><svg width="25px" height="25px" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14.128 6.276h-.503V6.25H8v2.5h3.532A3.748 3.748 0 0 1 4.25 7.5 3.75 3.75 0 0 1 8 3.75c.956 0 1.826.36 2.488.95l1.768-1.768A6.221 6.221 0 0 0 8 1.25a6.25 6.25 0 1 0 6.128 5.026Z" fill="#FFC107"></path><path d="m2.47 4.59 2.054 1.507A3.748 3.748 0 0 1 8 3.75c.956 0 1.826.36 2.488.95l1.768-1.768A6.221 6.221 0 0 0 8 1.25a6.246 6.246 0 0 0-5.53 3.34Z" fill="#FF3D00"></path><path d="M8 13.75a6.22 6.22 0 0 0 4.19-1.623l-1.934-1.637A3.722 3.722 0 0 1 8 11.25a3.748 3.748 0 0 1-3.526-2.483l-2.038 1.57A6.245 6.245 0 0 0 8 13.75Z" fill="#4CAF50"></path><path d="M14.128 6.276h-.503V6.25H8v2.5h3.532a3.762 3.762 0 0 1-1.277 1.74l1.935 1.637c-.137.125 2.06-1.502 2.06-4.627 0-.42-.043-.828-.122-1.224Z" fill="#1976D2"></path></svg><span class="font-primary text-[0.9rem]">Continue with Google</span></button>
    <h1>Login </h1>

    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" id='email' required />

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" id='psw' required />
    <button className="btn btn-success" type="submit">
      
          {formStatus}
        </button>
   
  </form>
</div>

        </div>
  )
}

export default Login