import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
// import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Link } from 'react-router-dom';

function NavScrollExample() {
  return (
    <Navbar style={{backgroundColor: "Dark"}} expand="lg">
      <Container fluid>
        <Navbar.Brand href="#">Online Mcq Quizlet</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Link to="/Home">Home</Link>
            <Nav.Link href="#action2">Expert solutions</Nav.Link>
            <NavDropdown title="Select Language" id="navbarScrollingDropdown">
              <NavDropdown.Item href="#action3">Hindi</NavDropdown.Item>
              <NavDropdown.Item href="#action4">
                English
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action5">
                Punjabi
              </NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Subjects" id="navbarScrollingDropdown">
              <NavDropdown.Item href="#action3">php</NavDropdown.Item>
              <NavDropdown.Item href="#action4">
                JavaScript
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action5">
                Android
              </NavDropdown.Item>
            </NavDropdown>
            </Nav>
          
          <Link to='/register'>  <Button variant="outline-success">Register</Button></Link>
        <Link to='/Login'>  <Button variant="outline-success">Login</Button></Link>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavScrollExample;